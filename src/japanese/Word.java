package japanese;

class Word {
	final String english;
	final String japanese;
	final String comment;

	public static Word from(String line) {
		if (line.startsWith("#") || line.isEmpty()) {
			return null;
		}
		String[] parts = line.split(",", 3);
		if (parts.length < 2) {
			System.out.println("Malformed line: " + line);
			return null;
		}
		String japanese = parts[0].trim();
		String english = parts[1].trim();
		String comment = parts.length > 2 ? parts[2].trim() : null;
		return new Word(english, japanese, comment);
	}

	public Word(String english, String japanese, String comment) {
		this.english = english;
		this.japanese = japanese;
		this.comment = comment;
	}

	@Override
	public String toString() {
		return String.format("%s | %s | %s", japanese, english, comment != null ? comment : "");
	}

}