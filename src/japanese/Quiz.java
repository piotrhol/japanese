package japanese;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Quiz {

	public static void run(Collection<Question> questions) {
		List<Question> questionsShuffled = new ArrayList<>(questions);
		Collections.shuffle(questionsShuffled);
		int good = 0;
		List<Question> badLines = new ArrayList<>();
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		for (Question q : questionsShuffled) {
			System.out.println(q.question);
			String answer = in.nextLine();
			if (answer.equals("X")) {
				break;
			}
			if (answer.trim().equalsIgnoreCase(q.answer)) {
				System.out.println("OK");
				good++;
			} else {
				System.out.println("Wrong: " + q.answer);
				badLines.add(q);
			}
			if (q.comment != null) {
				System.out.println("Comment: " + q.comment);
			}
			System.out.println("--------");
		}
		int bad = badLines.size();
		System.out.println(String.format("Correct: %d%% (%d/%d)", (int) (((double) good / (good + bad)) * 100), good,
				(good + bad)));
		if (bad > 0) {
			System.out.println();
			System.out.println("Incorrect questions:");
			for (Question line : badLines) {
				System.out.println(String.format("%s / %s", line.question, line.answer));
			}
		}
	}

	public static class Question {
		final String question;
		final String answer;
		final String comment;

		public Question(String question, String answer) {
			this(question, answer, null);
		}

		public Question(String question, String answer, String comment) {
			this.question = question;
			this.answer = answer;
			this.comment = comment;
		}

		@Override
		public String toString() {
			return String.format("%s | %s | %s", question, answer, comment != null ? comment : "");
		}

	}

}
