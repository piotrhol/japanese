package japanese;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import japanese.Quiz.Question;

public class Practice {

	enum Mode {
		WORDS_JAP, SENTENCES_JAP,
	}

	enum Special {
		TIME, PHONE_NUMBER, PRICE;
	}

	private static final Random RANDOM = new Random();
	private static final Pattern CURLY_PATTERN = Pattern.compile("\\{(\\w+)\\}");

	public static void main(String[] args) throws FileNotFoundException {
		if (args.length < 1) {
			throw new IllegalArgumentException("Expecting at least 1 parameter");
		}
		String filename = args[0];
		Mode mode = args.length > 1 ? Mode.valueOf(args[1]) : Mode.WORDS_JAP;
		List<String> lines = readFrom(filename);
		switch (mode) {
		case SENTENCES_JAP:
			sentencesPractice(lines);
			break;
		case WORDS_JAP:
		default:
			wordPractice(lines);
		}
	}

	private static void sentencesPractice(List<String> lines) {
		LinkedList<String> linesQ = new LinkedList<>(lines);
		List<Question> questions = loadQuestions(linesQ);
		Map<String, List<Word>> dictionary = loadDictionary(linesQ);
		List<Question> questions2 = swapPlaceholders(questions, dictionary);
		Quiz.run(questions2);
	}

	private static List<Question> swapPlaceholders(List<Question> questions, Map<String, List<Word>> dictionary) {
		List<Question> questions2 = new ArrayList<>();
		for (Question question : questions) {
			questions2.add(swapSpecials(swapDictionary(dictionary, question)));
		}
		return questions2;
	}

	private static Question swapDictionary(Map<String, List<Word>> dictionary, Question q) {
		Matcher m = CURLY_PATTERN.matcher(q.question);
		String newQ = q.question;
		String newA = q.answer;
		int cnt = 0;
		while (m.find()) {
			String key = m.group(1);
			List<Word> words = dictionary.get(key);
			if (words == null) {
				throw new IllegalArgumentException("Unknown key in: " + q);
			}
			Word word = words.get(RANDOM.nextInt(words.size()));
			newQ = newQ.replaceFirst("\\{" + Matcher.quoteReplacement(key) + "\\}", word.english);
			newA = newA.replaceFirst("\\{" + (cnt++) + "\\}", word.japanese);
		}
		return new Question(newQ, newA, q.comment);
	}

	private static Question swapSpecials(Question q) {
		String newQ = q.question;
		String newA = q.answer;
		for (Special s : Special.values()) {
			int cnt = 0;
			String key = s.name().toLowerCase();
			while (newQ.contains("[" + key + "]")) {
				String n = newSpecialValue(s);
				newQ = newQ.replaceFirst("\\[" + key + "\\]", n);
				newA = newA.replaceFirst("\\[" + (cnt++) + "\\]", n);
			}
		}
		return new Question(newQ, newA, q.comment);
	}

	private static String newSpecialValue(Special s) {
		switch (s) {
		case PHONE_NUMBER:
			return String.format("%03d-%03d-%04d", RANDOM.nextInt(1000), RANDOM.nextInt(1000), RANDOM.nextInt(10000));
		case PRICE:
			return "" + RANDOM.nextInt(10000);
		case TIME:
			return String.format("%02d:%02d", RANDOM.nextInt(24), RANDOM.nextInt(60));
		default:
			throw new AssertionError("Unsupported special: " + s);
		}
	}

	private static Map<String, List<Word>> loadDictionary(LinkedList<String> linesQ) {
		Map<String, List<Word>> dictionary = new HashMap<>();
		while (!linesQ.isEmpty()) {
			String key = linesQ.removeFirst();
			List<Word> words = new ArrayList<>();
			while (!linesQ.isEmpty()) {
				String line = linesQ.removeFirst();
				if (line.isEmpty()) {
					break;
				}
				words.add(Word.from(line));
			}
			dictionary.put(key, words);
		}
		return dictionary;
	}

	private static List<Question> loadQuestions(LinkedList<String> linesQ) {
		List<Question> questions = new ArrayList<>();
		while (!linesQ.isEmpty()) {
			String line = linesQ.removeFirst();
			if (line.isEmpty()) {
				break;
			}
			questions.add(new Question(line, linesQ.removeFirst()));
		}
		return questions;
	}

	private static void wordPractice(List<String> lines) {
		List<Question> q = new ArrayList<>();
		for (String line : lines) {
			Word word = Word.from(line);
			if (word == null) {
				continue;
			}
			q.add(new Question(word.japanese, word.english, word.comment));
		}
		Quiz.run(q);
	}

	private static List<String> readFrom(String filename) throws FileNotFoundException {
		List<String> lines = new ArrayList<>();
		try (Scanner sc = new Scanner(new File(filename))) {
			while (sc.hasNextLine()) {
				lines.add(sc.nextLine());
			}
		}
		return lines;
	}

}
